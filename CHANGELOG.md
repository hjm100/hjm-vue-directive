# 更新文档

## 1.0.0

1. 完善vue自定义指令项目结构
1. 添加手势指令
    1. swipeleft：左滑动
    1. swiperight：右滑动
    1. swipedown：下滑动
    1. swipeup：上滑动
    1. longtap：长按事件
    1. swiper：滑动事件
1. 添加加载指令
    1. imgload：图片加载（图片未完成加载前，用随机的背景色占位，图片加载完成后才直接渲染出来）