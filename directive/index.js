import touch from './touch'
import load from './load'

export default { touch, load }
