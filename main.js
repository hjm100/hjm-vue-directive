/* eslint-disable no-undef */
import Vue from 'vue'
import hjmvdirective from './directive/index'
// Vue.use(hjmvdirective.touch).use(hjmvdirective.load)
Object.keys(hjmvdirective).forEach(key => {
  Vue.use(hjmvdirective[key])
})
// app setup
export default new Vue({
  el: '#app',
  data: {
    imgs: [
      'https://hjm100.gitee.io/me/cdn/verdaccio/1.png',
      'https://hjm100.gitee.io/me/cdn/verdaccio/2.png',
      'https://hjm100.gitee.io/me/cdn/verdaccio/3.png'
    ]
  },
  methods: {
    swipeleft() {
      console.log('左滑动')
    },
    swiperight() {
      console.log('右滑动')
    },
    swipedown() {
      console.log('下滑动')
    },
    swipeup() {
      console.log('上滑动')
    },
    longtap() {
      console.log('长按事件')
    },
    swiper() {
      // 通过event拿到坐标初始量和移动量
      console.log(event.starDis)
      console.log(event.moveDis)
      console.log('我是移动事件')
    }
  }
})
